---
title: "Sumaco Year1 Paper"
author: "Various Authors"
date: "Last Modified 12/02/2022"
output:
  md_document:
    variant: markdown_github
---

#Load Main Packages
```{r}
library(phyloseq)
library(ggplot2)
library(vegan)
```

#Load Data
```{r}
#Read OTUS Count Table
mothur_count_table=read.csv("./data/mothur_count_table_SUM-Year1_OTUS.csv",row.names = 1,check.names = F)
#Read Taxa Count Table
mothur_tax_table=read.csv("./data/mothur_tax_table_SUM-Year1_OTUS.csv",row.names = 1,check.names = F)

#Make Phyloseq object
#Pre-format 
##Taxa
TAX=tax_table(as.matrix(mothur_tax_table))
##Count_data as matrix
COUNT=otu_table(as.matrix(mothur_count_table),taxa_are_rows = T)
##Transform to phyloseq object 
mothur_pseq=phyloseq(TAX, COUNT)
mothur_pseq
```

```{r}
#Lets see how many original seqs
seq_init=sum(otu_table(mothur_pseq))
seq_init
```

#Filter Data: Noise
```{r}
#Filter Unknows at Kingdom level
mothur_data_fU1=subset_taxa(mothur_pseq,Kingdom != "unknown")
#Filter Unclassified Bacteria
mothur_data_fU1_fU2=subset_taxa(mothur_data_fU1,Phylum != "Bacteria_unclassified")
#Filter Archaea
mothur_data_fU1_fU2_fU3=subset_taxa(mothur_data_fU1_fU2,Kingdom != "Archaea")
mothur_data_fU1_fU2_fU3
```

```{r}
#Lets see how many seqs left
seq_post=sum(otu_table(mothur_data_fU1_fU2_fU3))
seq_post
```

#Filter Data: Singletons
```{r}
#Filter Singletons
mothur_data_fS_fU123=prune_taxa(taxa_sums(mothur_data_fU1_fU2_fU3)>1,mothur_data_fU1_fU2_fU3)
mothur_data_fS_fU123
```

```{r}
#Lets see how many seqs left
seq_post2=sum(otu_table(mothur_data_fS_fU123))
seq_post2
```

#Make Final Phyloseq Objects
```{r}
#All samples
##Metadata
metadata_s = read.table("./data/metadata_shared.csv",sep=",",header = TRUE,check.names = F)
rownames(metadata_s)=metadata_s$Sample
##Make all samples Phyloseq
mothur_otu_all=mothur_data_fS_fU123
sample_data(mothur_otu_all) = sample_data(metadata_s)

#Soil samples
##Metadata
metadata_soil=read.table("./data/metadata_soil.csv",sep=",",header = TRUE,check.names = F)
rownames(metadata_soil)=metadata_soil$Sample
metadata=merge(metadata_s,metadata_soil,by="Sample")
rownames(metadata)=metadata$Sample
##Select samples
mothur_otu_soil=prune_samples(data.frame(sample_data(mothur_otu_all))[,"Type"]=="Soil",mothur_otu_all)
sample_data(mothur_otu_soil) = sample_data(metadata)
##filter zeros again
mothur_otu_soil=prune_taxa(taxa_sums(mothur_otu_soil)>0,mothur_otu_soil)
```

#Stats- All after filters
```{r}
SeqDepth = colSums(otu_table(mothur_otu_all))
#Total seqs
print("Seqs number:")
sum(SeqDepth)
#Summary stats
summary(SeqDepth)
SeqDepth
```

#Stats- Soil
```{r}
SeqDepth = colSums(otu_table(mothur_otu_soil))
#Total seqs
print("Seqs number:")
sum(SeqDepth)
#Summary stats
summary(SeqDepth)
SeqDepth
sd(SeqDepth)
```

#Get genus, family and phylum soil objects
```{r}
mothur_ge_soil=tax_glom(mothur_otu_soil, "Genus")
mothur_ge_soil
```

```{r}
mothur_fa_soil=tax_glom(mothur_otu_soil, "Family")
mothur_fa_soil
```

```{r}
mothur_phy_soil=tax_glom(mothur_otu_soil, "Phylum")
mothur_phy_soil
```

#Figure2 (General Description)

```{r}
library(viridis)
#A) Rare-curves at Genus level (Soil samples)
plt_refraction <- ranacapa::ggrare(mothur_ge_soil, step = 600, color = 'Altitude',
                 label = "Sample", se = F, plot = F) 
plt_refraction <-  plt_refraction + xlim(c(0, 60000)) + theme_test() +
  scale_color_viridis() +ylab("Genus Richness")
plt_refraction$layers[[1]] <- NULL
#Get final Plot
plt_refraction
```

```{r}
#B) Alpha Diversity at Genus Level
richnes_estimates=estimate_richness(mothur_ge_soil)

#Format plot data
alpha_plot_data <- richnes_estimates[,c("Shannon", "Simpson")]
alpha_plot_data <- merge(alpha_plot_data,data.frame(sample_data(mothur_ge_soil)),by=0)
alpha_plot_data=alpha_plot_data[,c("Sample","Shannon","Simpson","Altitude_group")]
alpha_plot_data=reshape2::melt(alpha_plot_data,ids = c("Sample","Altitude_group"))

#Make plot
alpha_plot=ggplot(alpha_plot_data,aes(x=Altitude_group,y=value)) + 
  geom_boxplot(aes(fill = Altitude_group), alpha = 0.8, notch = F) +
  facet_wrap("variable", scales = "free_y") +theme_test()+ scale_fill_brewer(palette="Set1",direction = -1)+labs(y = "",x="",fill="Altitude")
#Get final plot
alpha_plot=alpha_plot+ggpubr::stat_compare_means(method = "wilcox.test",method.args = list(alternative = "great"))
```

```{r}
#C) Simpson vs Altitude at Genus Level
library(MASS)
#Format data
#Format plot data
soil_lm_data <- merge(richnes_estimates,data.frame(sample_data(mothur_ge_soil)),by=0)
soil_lm_data=soil_lm_data[,c("Sample","Simpson","Altitude_group","Altitude")]

# Create the linear model
soil_lm <-  MASS::rlm(Simpson ~ Altitude, data = soil_lm_data)
sfsmisc::f.robftest(soil_lm, var = "Altitude")

#Plot
plt_sl_rlm <- ggplot(soil_lm_data, aes(Altitude, Simpson)) +
 stat_smooth(method = "rlm", col = "#4d0026", size = 0.5) +
  geom_point(aes(color = Altitude_group), size = 3) + 
  scale_color_brewer(palette="Set1",direction = -1)+
  theme_test() +
  #annotate("text",label = "p-value = 0.60", x = 5320, y = 290) +
  labs(x = "Altitude (m.a.s.l)", y = "Alpha Diversity (Simpson)",color="Altitude") 
#Get final plot
plt_sl_rlm
```

```{r}
#D) Composition Plot at Phylum Level (Top 8 Phyla)
#Convert to proportions
mothur_soil_phy_prop=microbiome::transform(mothur_phy_soil,"compositional")
#Get top 8 taxa
top8_phy=microbiome::top_taxa(mothur_soil_phy_prop,n=8)
mothur_soil_phy_prop.top8=prune_taxa(top8_phy,mothur_soil_phy_prop)
#See how much relative abundance per sample
colSums(otu_table(mothur_soil_phy_prop.top8))

#Construct table
pp=psmelt(physeq = mothur_soil_phy_prop.top8)
pp$Sample_Alt=paste(pp$Sample,'\n',pp$Altitude,sep ="")

#Plot
plot_bar1= ggplot(pp, aes(x = reorder(Sample_Alt,-Altitude), y = Abundance, fill = Phylum))+
geom_bar(stat = "identity", color = "black", width = 0.9)  +
labs(y = "Relative Abundance",x="") + theme_bw()+
theme(axis.text.x = element_text(angle=90, hjust=1, vjust=.5))

#Get final plot
comp_barplot1=plot_bar1+facet_wrap(~Altitude_group,scales = "free_x")+scale_fill_brewer(palette="Set2")
```

```{r}
#E) Composition Plot at Family Level (Top 15 Families)
#Convert to proportions
mothur_soil_fam_prop=microbiome::transform(mothur_fa_soil,"compositional")
#Get top 15 taxa
top15_fam=microbiome::top_taxa(mothur_soil_fam_prop,n=15)
mothur_soil_fam_prop.top15=prune_taxa(top15_fam,mothur_soil_fam_prop)
#See how much relative abundance per sample
colSums(otu_table(mothur_soil_fam_prop.top15))

#Construct table
pp2=psmelt(physeq = mothur_soil_fam_prop.top15)
pp2$Taxonomy=paste(pp2$Phylum,pp2$Class,pp2$Order,pp2$Family,sep =":")
pp2$Sample_Alt=paste(pp2$Sample,'\n',pp2$Altitude,sep ="")

#Plot
plot_bar2= ggplot(pp2, aes(x = reorder(Sample_Alt,-Altitude), y = Abundance, fill = Taxonomy))+
geom_bar(stat = "identity", color = "black", width = 0.9)  +
labs(y = "Relative Abundance",x="",fill="Family") + theme_bw()+
theme(axis.text.x = element_text(angle=90, hjust=1, vjust=.5))

# Define the number of colors you want
library(RColorBrewer)
nb.cols <- 15
mycolors <- colorRampPalette(brewer.pal(8, "Set3"))(nb.cols)

#Get final plot
comp_barplot2=plot_bar2+facet_wrap(~Altitude_group,scales = "free_x")+scale_fill_manual(values = mycolors)
```

```{r}
#Combine plots make Figure 2
##Load library
library(cowplot)
top_fig2=plot_grid(plt_refraction,alpha_plot,plt_sl_rlm,labels = c('a)','b)','c)'), nrow = 1,rel_widths = c(1,1.4,1))
bot_fig2=plot_grid(comp_barplot1,comp_barplot2,labels = c('d)','e)'),rel_heights = c(1,1),rel_widths = c(0.6,1),align = "h")
fig2=plot_grid(top_fig2,bot_fig2,nrow = 2,rel_heights = c(0.8,1),rel_widths = c(1,0.2))
```

```{r,fig.width=16,fig.height=8.5}
#Show final figure
fig2
```

```{r}
#Save plot
ggsave(plot=fig2,filename = "./Figure2.pdf",height = 8.5, width = 16,scale = 1)
ggsave(plot=fig2,filename = "./Figure2.png",height = 8.5, width = 16,scale = 1)
```

#Figure3 (CCA Analysis)

```{r}
#Get Families from Top 8 Phyla
##Top 8 Phyla list
top8_phyla_names=as.vector(tax_table(mothur_soil_phy_prop.top8)[,"Phylum"])
##Get genera counts from top 8 phyla 
mothur_ge_soil_top8phy=prune_taxa(tax_table(mothur_ge_soil)[,"Phylum"] %in% top8_phyla_names,mothur_ge_soil)
mothur_ge_soil_top8phy
```

```{r}
#Prepare data for analysis####

##Extracting the community matrix from th Phyloseq object
Community_matrix = as(otu_table(mothur_ge_soil_top8phy), "matrix")
Community_matrix = t(Community_matrix)
Community_matrix = as.data.frame(Community_matrix)

##Extracting the taxonomic matrix from the Phyloseq object
Taxonomy_id = as(tax_table(mothur_ge_soil_top8phy), "matrix")
Taxonomy_id = as.data.frame(Taxonomy_id)

##Extracting the environmental vectors from the Phyloseq object
Environmental_vectors = as(sample_data(mothur_ge_soil_top8phy), "matrix")
Environmental_vectors = as.data.frame(Environmental_vectors)

##Assigning names to the genera in the community matrix
names(Community_matrix)=Taxonomy_id$Genus

#Prepare environmental vectors
Environmental_vectors_def=merge(Environmental_vectors,richnes_estimates[,c("Observed","Simpson")],by=0)
row.names(Environmental_vectors_def)=Environmental_vectors_def$Sample
Environmental_vectors_def=Environmental_vectors_def[,c("Altitude","pH","EC","Ca2+","Mg2+","Na+","K+","Fe","Mn2+","P","S","N","Org","Hum","CEC","Observed","Simpson")]
colnames(Environmental_vectors_def)[16]="Richness"
#Check if they are all numeric(if not transform)
#sapply(Environmental_vectors_def, class)
#Convert to numeric
cols.num=c("Altitude","pH","EC","Ca2+","Mg2+","Na+","K+","Fe","Mn2+","P","S","N","Org","Hum","CEC")
Environmental_vectors_def[cols.num] = sapply(Environmental_vectors_def[cols.num], as.numeric)
```

##CCA
```{r}
##CCA Model
CCA_analysis = cca (Community_matrix ~ Altitude + pH + P + S + `Mn2+` + CEC, data = Environmental_vectors_def)
```

```{r}
##Colinearity test for environmental constraints in the CCA model
vif.cca(CCA_analysis)
```

```{r}
##Stepwise selection in the CCA model ####
#[model with just the intercept]
mod0 = cca (Community_matrix ~ 1, data = Environmental_vectors_def)
##ordistep using [model with six constraints]
mod <- ordistep(mod0, scope = formula(CCA_analysis), direction='both', trace=1)
```


```{r}
#Results of the stepwise selection
#The procedure chooses only two constraints: S and Altitude. The spatial configuration is no different from the configuration with six variables.####
mod$anova
plot(mod)
CCA_analysis_2cons = cca (Community_matrix ~ Altitude + S, data = Environmental_vectors_def)
CCA_analysis_2cons
```

##PERMANOVA with the square root of dissimilarities (Bray-Curtis distances). Tests for the significance of the different terms of the model (the environmental variables).
```{r}
#Significance of all terms together. ####
adonis2 (Community_matrix ~ Altitude + pH + P + S + `Mn2+` + CEC, data = Environmental_vectors_def, method = 'bray', by = NULL, sqrt.dist = TRUE)
```

```{r}
#Significance of terms, sequentially. Elevation and SO4 are significant.####
adonis2 (Community_matrix ~ Altitude + pH + P + S + `Mn2+` + CEC, data = Environmental_vectors_def, method = 'bray', by = 'terms', sqrt.dist = TRUE)
```

```{r}
#Significance of the marginal effect of each term. Only SO4 is significant!####
adonis2 (Community_matrix ~ Altitude + pH + P + S + `Mn2+` + CEC, data = Environmental_vectors_def, method = 'bray', by = 'margin', sqrt.dist = TRUE)
```

##Some diagnostics on partialled-out variables for the CCA model

```{r}
#CCA six factors, Altitude conditioned. The pattern of the model is lost if Elevation is partialed out.####
CCA_analysis_AltitudeCond = cca (Community_matrix ~ pH + P + S + `Mn2+` + CEC + Condition (Altitude), data = Environmental_vectors_def)
CCA_analysis_AltitudeCond
ordiplot (CCA_analysis_AltitudeCond)
ordihull(CCA_analysis_AltitudeCond, Environmental_vectors_def$Altitude, draw = "lines")
```

```{r}
#CCA six factors, Altitude conditioned. The pattern of the model is lost if SO4 is partialed out####
CCA_analysis_SCond = cca (Community_matrix ~ Altitude + pH + P  + `Mn2+` + CEC + Condition (S), data = Environmental_vectors_def)
CCA_analysis_SCond
ordiplot (CCA_analysis_SCond)
ordihull(CCA_analysis_SCond, Environmental_vectors_def$Altitude, draw = "lines")
```

##Final CCA diagnostics and figures

```{r}
ordiplot(CCA_analysis)
ordihull(CCA_analysis, Environmental_vectors_def$Altitude, draw = "lines")
ordipointlabel(CCA_analysis, display = "sites", add = TRUE)
```

```{r}
##The corresponding Figure3 was latter manually curated by Pablo Jarrín-V####
summary(eigenvals(CCA_analysis))
ordiplot (CCA_analysis)
ordihull(CCA_analysis, Environmental_vectors_def$Altitude, draw = "lines")
ordipointlabel(CCA_analysis, display = "sites", add = TRUE)
ordilabel(CCA_analysis, display = ("species"))
ordilabel(CCA_analysis, display = ("sites"))
ordipointlabel(CCA_analysis, display = "sites", add = TRUE)
ordipointlabel(CCA_analysis, display = "species", add = TRUE)
orditkplot(CCA_analysis, display = "species", add = TRUE)
orditorp(CCA_analysis, display = "species", select = TRUE, add = TRUE, air = 1, pch = NULL)
```

```{r}
#CCA Diagnostics
anova(CCA_analysis)
anova(CCA_analysis, by='term', permutations = 9000)
anova(CCA_analysis, by='axis', permutations = 9000)
anova(CCA_analysis, by='margin', permutations = 9000)
```

#3D CCA(Supplementary Video1)

```{r}
library(rgl)
library(magick)

#3D CCA(Supplementary Video1)
#groups
groups_vegan = (rep(c(2,4), times = c(5,4)))
#new row names
Community_matrix_3D = Community_matrix
row.names(Community_matrix_3D) = c('SUM002-3783','SUM003-3808','SUM004-3809','SUM005-2998','SUM006-2990', 'SUM008-2261', 'SUM009-1953', 'SUM010-1942', 'SUM011-1584')
#Get model [equal to CCA_analysis, but with Custom Sample Names]
CCA_analysis_3d = cca (Community_matrix_3D ~ Altitude + pH + P + S + `Mn2+` + CEC, data = Environmental_vectors_def)
#3D plot
#A temporary directory where the supplementary video1 will be saved
temp.dir <- tempdir ()
#Setting up colors for the 3D CCA figure
my.color = (rep(c("deepskyblue2", "red"), times = c(5,4)))
#The 3D CCA figure
vegan3d::ordirgl (CCA_analysis_3d, display = 'sites', type = 't', groups=groups_vegan, col = my.color, ax.col = 9, arr.col = "green", kind='hull')
#Making a movie on the 3D CCA figure, the .GIF file will be saved in "temp.dir", call "tempdir ()" to find its location in the drive. 
movie3d(spin3d(axis = c(0,1,0)), duration=60/5, movie = "ordirgl", fps = 20, dir = temp.dir)
```

#Figure4 (Environmental constrains)

```{r}
library (corrplot)

#Pearson correlation
Matrix_correlation = cor (Environmental_vectors_def)

#Significance test matrix for Pearson correlation
Matrix_correlation1_sig = cor.mtest (Environmental_vectors_def, conf.level = .95)

#Correlogram
corrplot(Matrix_correlation, method = 'ellipse', type = "full", order = "hclust", hclust.method = 'average', addrect = 5, cl.pos = "b", p.mat = Matrix_correlation1_sig$p, sig.level = .05, insig = 'label_sig', pch.cex = 1, pch.col = 'red', col = c("black", "white"), bg = "grey", tl.col = 'black')
```

```{r}
#Save plot as PDF
pdf(file = "./Figure4.pdf")
corrplot(Matrix_correlation, method = 'ellipse', type = "full", order = "hclust", hclust.method = 'average', addrect = 5, cl.pos = "b", p.mat = Matrix_correlation1_sig$p, sig.level = .05, insig = 'label_sig', pch.cex = 1, pch.col = 'red', col = c("black", "white"), bg = "grey", tl.col = 'black')
dev.off()

#Save plot as png
png(file = "./Figure4.png",width = 680, height = 680,units = "px", pointsize = 12)
corrplot(Matrix_correlation, method = 'ellipse', type = "full", order = "hclust", hclust.method = 'average', addrect = 5, cl.pos = "b", p.mat = Matrix_correlation1_sig$p, sig.level = .05, insig = 'label_sig', pch.cex = 1, pch.col = 'red', col = c("black", "white"), bg = "grey", tl.col = 'black')
dev.off()
```


#Figure5 (The six environmental constraints)

```{r}
#relaimpo package: calculates several relative importance metrics for the linear model####

library(relaimpo)

#Get six constraints
model_to_test = calc.relimp(Richness ~ Altitude + pH + P + S + `Mn2+` + CEC, Environmental_vectors_def, type = c('lmg'), rela = TRUE)

print(model_to_test)
plot (model_to_test)
```

```{r}
#Partial regresions analysis
##Residual effect of each selected environmental constraint on total richness by partial-regression plots for the assessed six-constraint linear model, as implemented in the avPlots algorithm of the CAR package####
library(car)
library(skimr)
library(tidyverse)
#Lineal model
model_to_test1 = lm (Richness ~ Altitude + pH + P + S + `Mn2+` + CEC, Environmental_vectors_def)
#Various graphics
qqPlot(model_to_test1,labels = row.names(data),id.method = "identify",
       simulate= TRUE, main='QQ PLOT')
vif(model_to_test1)
crPlots (model_to_test1, id=TRUE, smooth = FALSE, col.lines = ('black'), grid = FALSE)
#ceresPlot(model_to_test1)
residualPlots(model_to_test1)
densityPlot (model_to_test1$residuals)
durbinWatsonTest (model_to_test1)
```

```{r}
#Get lm model for the final graphic
model_to_test1 = lm (Richness ~ Altitude + pH + P + S + `Mn2+` + CEC, Environmental_vectors_def)
avPlotsmatrix=avPlots (model_to_test1)#this test is the one applied. id=list(method='identify', n=9)
mcPlots (model_to_test1)#this test is the one that shows the process by which raw data is adjusted to avPlots
```

```{r}
#Get data.frames info
Altura_avplot = as.data.frame(avPlotsmatrix$Altitude)
pH_avplot = as.data.frame(avPlotsmatrix$pH)
P_avplot = as.data.frame(avPlotsmatrix$P)
S_avplot = as.data.frame(avPlotsmatrix$S)
Mn_avplot = as.data.frame(avPlotsmatrix[5])
CEC_avplot = as.data.frame(avPlotsmatrix$CEC)

#Set percentages from model_to_test
per_Alt=paste(round(0.08102559*100),"%",sep="")
per_pH=paste(round(0.11415175*100),"%",sep="")
per_P=paste(round(0.09277148*100),"%",sep="")
per_S=paste(round(0.28147439*100),"%",sep="")
per_Mn=paste(round(0.41147188*100),"%",sep="")
per_CEC=paste(round(0.01910491*100),"%",sep="")
```

```{r}
#Mn2+
g1=ggplot (Mn_avplot, aes(Mn_avplot[, 1], Mn_avplot[, 2])) + geom_smooth(method = lm, linetype="dashed", color = 'black', size = 2, se = TRUE)  + geom_point (shape = 21, size=4, color = 'black', fill = 'gray62', stroke = 2) +  theme_classic()+labs(x="Mn2+ | Others",y="Richness | Others")+annotate("text", x = -1.5, y = 50, label = per_Mn,size = 10)+theme(text = element_text(size = 20))
#S
g2=ggplot (S_avplot, aes(S_avplot[, 1], S_avplot[, 2])) + geom_smooth(method = lm, linetype="dashed", color = 'black', size = 2, se = TRUE)  + geom_point (shape = 21, size=4, color = 'black', fill = 'gray62', stroke = 2) +  theme_classic()+labs(x="S | Others",y="Richness | Others")+annotate("text", x = 0.6, y = 50, label = per_S,size = 10)+theme(text = element_text(size = 20))
#pH
g3=ggplot (pH_avplot, aes(pH_avplot[, 1], pH_avplot[, 2])) + geom_smooth(method = lm, linetype="dashed", color = 'black', size = 2, se = TRUE)  + geom_point (shape = 21, size=4, color = 'black', fill = 'gray62', stroke = 2) +  theme_classic()+labs(x="pH | Others",y="Richness | Others")+annotate("text", x = -0.015, y = 40, label = per_pH,size = 10)+theme(text = element_text(size = 20))
#P
g4=ggplot (P_avplot, aes(P_avplot[, 1], P_avplot[, 2])) + geom_smooth(method = lm, linetype="dashed", color = 'black', size = 2, se = TRUE)  + geom_point (shape = 21, size=4, color = 'black', fill = 'gray62', stroke = 2) +  theme_classic()+labs(x="P | Others",y="Richness | Others")+annotate("text", x = 0.1, y = 55, label = per_P,size = 10)+theme(text = element_text(size = 20))
#Altitude
g5=ggplot (Altura_avplot, aes(Altura_avplot[, 1], Altura_avplot[, 2])) + geom_smooth(method = lm, linetype="dashed", color = 'black', size = 2, se = TRUE)+geom_point (shape = 21, size=4, color = 'black', fill = 'gray62', stroke = 2) + theme_classic()+labs(x="Altitude | Others",y="Richness | Others")+annotate("text", x = 70, y = 60, label = per_Alt,size = 10)+theme(text = element_text(size = 20))
#CEC
g6=ggplot (CEC_avplot, aes(CEC_avplot[, 1], CEC_avplot[, 2])) + geom_smooth(method = lm, linetype="dashed", color = 'black', size = 2, se = TRUE)+geom_point (shape = 21, size=4, color = 'black', fill = 'gray62', stroke = 2) + theme_classic()+labs(x="CEC | Others",y="Richness | Others")+annotate("text", x = -0.1, y = 40, label = per_CEC,size = 10)+theme(text = element_text(size = 20))+xlim(-0.7,0.5)
```

```{r}
#Combine plots make Figure 5
##Load library
library(cowplot)
fig5=plot_grid(g1,g2,g3,g4,g5,g6, nrow = 3, ncol = 2)
```

```{r,fig.width=10,fig.height=16}
#Show final figure
fig5
```

```{r}
#Save plot
ggsave(plot=fig5,filename = "./Figure5.pdf",height = 16, width = 10,scale = 1)
ggsave(plot=fig5,filename = "./Figure5.png",height = 16, width = 10,scale = 1)
```

#Figure6 (25 most abundant soil families)

```{r}
#Get top 25 Families
top25_fam=microbiome::top_taxa(mothur_soil_fam_prop,n=25)
mothur_soil_fam_prop.top25=prune_taxa(top25_fam,mothur_soil_fam_prop)
#See how much relative abundance per sample
colSums(otu_table(mothur_soil_fam_prop.top25))
```

```{r}
#Get inputs

##Get Proportions for top 25 families
top25_fam_prop=data.frame(otu_table(mothur_soil_fam_prop.top25))
##Change taxa names to full taxonomy
top25_fam_tax=data.frame(tax_table(mothur_soil_fam_prop.top25))
row.names(top25_fam_prop)=paste(top25_fam_tax$Class,top25_fam_tax$Order,top25_fam_tax$Family,sep =":")
##Change samples names
samples_data=data.frame(sample_data(mothur_soil_fam_prop.top25))
colnames(top25_fam_prop)=paste(samples_data$Sample,'\n',samples_data$Altitude,sep ="")

##Get phylum labels info(taxa) and set colors
phy_info=top25_fam_tax$Phylum
phy_colors=c(Acidobacteria="#66C2A5",Actinobacteria="#FC8D62", Bacteroidetes="#8DA0CB", Chloroflexi="#E78AC3", Planctomycetes="#FFD92F",Proteobacteria="#E5C494", Verrucomicrobia="#B3B3B3" )

##Get altitude labels (samples) and set colors
alt_info=data.frame(sample_data(mothur_soil_fam_prop.top25))$Altitude_group
alt_colors=c(Low="#E41A1C",High="#377EB8")
```

```{r}
library(ComplexHeatmap)
#Get Top annotations 
top_info <- HeatmapAnnotation(gp = gpar(col = "black"),show_annotation_name = T,show_legend=F,
                              Altitude=alt_info,col = list(Altitude=alt_colors))
#Get Lateral annotations 
lateral_info <-rowAnnotation(Phylum=phy_info, show_annotation_name = T,show_legend=F,
                          col=list(Phylum=phy_colors),gp = gpar(col = "black"))
#Color Function for Heatmap
col3=circlize::colorRamp2(c(0,0.001,0.005,0.01,0.02,0.05,0.1,0.15,0.2), c(rev(RColorBrewer::brewer.pal(n = 9, name = "RdYlBu"))))
#col3=circlize::colorRamp2(c(0,0.001,0.005,0.01,0.02,0.05,0.1,0.15,0.2), c(viridis::viridis(9)))

#Heatmap
htA2<-Heatmap(top25_fam_prop,show_column_names = T,
              col = col3,
              border_gp = grid::gpar(col = "black", lty = 1),
              rect_gp = grid::gpar(col = "black", lwd = 2),
              #clustering_distance_rows = "euclidean",clustering_method_rows = "complete",
              clustering_distance_columns = "euclidean",clustering_method_columns = "complete",
              show_heatmap_legend = F,row_names_max_width = unit(15, "cm"),
              top_annotation = top_info,
              row_km = 3, row_km_repeats = 100,
              #column_km = 4, column_km_repeats = 100,
              right_annotation = lateral_info)

#Convert to ggplot
gb_ht=grid.grabExpr(draw(htA2))
gb_ht=ggpubr::as_ggplot(gb_ht)
gb_ht

#Create legend in the desired order
#Create and combine legends
lgd1=Legend(col_fun = col3,title="Relative Abundance",at=c(0,0.005,0.01,0.02,0.05,0.1,0.15,0.2),legend_width = unit(8, "cm"),direction = "horizontal")
lgd2=Legend(labels = names(alt_colors),legend_gp = gpar(fill=alt_colors),title = "Altitude")
lgda=Legend(labels = names(phy_colors),legend_gp = gpar(fill=phy_colors),title = "Phylum",ncol = 2)
pd = packLegend(lgd1, lgd2, lgda,direction = "horizontal")
#Convert to ggplot
gb_L= grid.grabExpr(draw(pd))
gg_L=ggpubr::as_ggplot(gb_L)
gg_L
fig6<-cowplot::plot_grid(gg_L,gb_ht,nrow = 2,rel_heights = c(0.6,5))
```

```{r,fig.width=12,fig.height=9}
#Show final figure
fig6
```

```{r}
#Save plot
ggsave(plot=fig6,filename = "./Figure6.pdf",height = 9, width = 12,scale = 1)
ggsave(plot=fig6,filename = "./Figure6.png",height = 9, width = 12,scale = 1)
```

#Figure7 (Vulcano plot Soil- Altitudes)

```{r}
#1) Do the main test analysis
library(DESeq2)
#Convert to deseq2 object
diagdds = phyloseq_to_deseq2(mothur_ge_soil, ~ Altitude_group)
# calculate geometric means prior to estimate size factors
gm_mean = function(x, na.rm=TRUE){exp(sum(log(x[x > 0]), na.rm=na.rm) / length(x))}
geoMeans = apply(counts(diagdds), 1, gm_mean)
#Estimate sizefactors and dispersions
diagdds = estimateSizeFactors(diagdds, geoMeans = geoMeans)
diagdds = estimateDispersions(diagdds, fitType='local')
#Set reference
diagdds$Altitude_group = relevel(diagdds$Altitude_group , ref = "High")
#Do DESEQ Testing
diagdds = DESeq(diagdds, fitType="local")
#Get results and filter NAs
res_soil = results(diagdds)
res_soil = res_soil[order(res_soil$padj, na.last = NA), ]
#Add taxonomy info
res_soil <- cbind(as(res_soil,"data.frame"),as(tax_table(mothur_ge_soil)[rownames(res_soil), ],"matrix"))
```

```{r}
#2) Process results and get plot
#Get extra data.frame to play
res_df = as.data.frame(res_soil)
#res_df = res_df[res_df$baseMean >11,]
#Get column por Significance
##Set Initial Default value
res_df$diffexpressed <- "No significant"
## if log2Foldchange > 0.6 and pvalue < 0.05, set as "UP" / Low Altitude
res_df$diffexpressed[res_df$log2FoldChange > 0.6 & res_df$padj < 0.05] <- "Low  Altitude"
# if log2Foldchange < -0.6 and pvalue < 0.05, set as "DOWN" / High Altitude
res_df$diffexpressed[res_df$log2FoldChange < -0.6 & res_df$padj < 0.05] <- "High Altitude"

#Remove unknown genera (_unclassified, _ge, uncultured) from data.frame
res_df=res_df[!grepl(pattern = "_unclassified",x = res_df$Genus),]
res_df=res_df[!grepl(pattern = "_ge",x = res_df$Genus),]
res_df=res_df[!grepl(pattern = "uncultured",x = res_df$Genus),]

#Now we trunc insignificant points por labels(turn to NAs)
res_df[res_df$diffexpressed == "No significant", "Genus"] = NA

library(ggrepel)
# plot adding up all layers we have seen so far
fig7 = ggplot(data=res_df, aes(x=log2FoldChange, y=-log10(padj), col=diffexpressed))+geom_vline(xintercept=c(-0.6, 0.6), col="grey",linetype="dashed") + geom_hline(yintercept=-log10(0.05), col="grey",linetype="dashed") + geom_point() + theme_classic() 
# Add text labels
fig7 = fig7 + geom_text_repel(aes(label=Genus),box.padding = 0.5,force = 20,seed = 42,point.padding=0.5,force_pull = 0.5,max.overlaps = Inf,show.legend = F)
# Change colors
fig7=fig7+scale_color_manual(values=c("#377EB8", "#E41A1C", "#FFD92F"))
#Change legend title
fig7=fig7+guides(color=guide_legend("  Differential\n  Abundance"))
```

```{r,fig.width=9,fig.height=7}
#Show final figure
fig7
```

```{r}
#Save plot
ggsave(plot=fig7,filename = "./Figure7.pdf",height = 7, width = 9,scale = 1)
ggsave(plot=fig7,filename = "./Figure7.png",height = 7, width = 9,scale = 1)
```
